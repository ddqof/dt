package me.ddqof.dt

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LifecycleEventObserver

/**
 * Базовая [android.app.Activity] логирующая свой жизненный цикл
 */
abstract class LifecycleLoggedActivity : AppCompatActivity() {
    companion object {
        const val NUMBER_DATA_KEY = "contentNumber"
    }

    private val activityTag: String = javaClass.simpleName
    private val lifecycleEventObserver = LifecycleEventObserver { _, event ->
        Log.i(
            activityTag,
            "$activityTag lifecycle was changed by event: $event"
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycle.addObserver(lifecycleEventObserver)
    }
}
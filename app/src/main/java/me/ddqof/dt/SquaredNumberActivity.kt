package me.ddqof.dt

import android.os.Bundle
import me.ddqof.dt.databinding.SquaredNumberActivityBinding

/**
 * [android.app.Activity] отображающее квадрат числа, которое
 * отображалось на [IncrementingNumberActivity]
 */
class SquaredNumberActivity : LifecycleLoggedActivity() {
    private lateinit var binding: SquaredNumberActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = SquaredNumberActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val number = intent.getIntExtra(NUMBER_DATA_KEY, 0)
        with(binding.squaredNumber) {
            text = text.toString().format(number * number)
        }
    }
}
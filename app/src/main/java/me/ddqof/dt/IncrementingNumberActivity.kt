package me.ddqof.dt

import android.content.Intent
import android.os.Bundle
import android.view.View
import me.ddqof.dt.databinding.IncrementingNumberActivityBinding

/**
 * [android.app.Activity] отображающая число, которое увеличивается на 1
 * при **любой** смене конфигурации
 */
class IncrementingNumberActivity : LifecycleLoggedActivity() {
    private var number: Int = -1
    private lateinit var binding: IncrementingNumberActivityBinding

    /**
     * По умолчанию при смене конфигурации Acitivity пересоздается, поэтому вызывается метод [onCreate].
     * В нем определяем логику инкремента числа при смене конфигурации.
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = IncrementingNumberActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        number = savedInstanceState?.getInt(NUMBER_DATA_KEY) ?: number
        with(binding.incrementingNumber) {
            text = text.toString().format(++number)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.run { putInt(NUMBER_DATA_KEY, number) }
        super.onSaveInstanceState(outState)
    }

    @Suppress("UNUSED_PARAMETER")
    fun showSquare(view: View) = startActivity(
        Intent(this, SquaredNumberActivity::class.java).apply {
            putExtra(NUMBER_DATA_KEY, number)
        }
    )
}